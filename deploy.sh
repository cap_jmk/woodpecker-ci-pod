mkdir -p ./woodpecker-server-data ./woodpecker-agent-config

podman secret create woodpecker-agent-secret .agent-secret
podman secret create woodpecker-forgejo-secret .forgejo-secret

bash pod.sh

bash woodpecker.sh
bash agent.sh
#bash pg.sh
#bash pg-admin.sh

echo
echo "The pods running are"
podman pod ls
echo
echo "The containers running are"
podman ps -a

#bash generate-deamons.sh
#bash start-deamons.sh