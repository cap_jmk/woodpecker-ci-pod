# woodpecker-ci-pod

Pods for deploying Woodpecker CI alongside with forgejo using podman


## Encrypt secrets 

https://woodpecker-ci.org/docs/administration/encryption

It seems totally possible to solely use podman on the remote system by hosting a container there 


Or better from the docs: 


While the agent was developed with Docker/Moby, Podman can also be used by setting the environment variable DOCKER_HOST to point to the Podman socket. In order to work without workarounds, Podman 4.0 (or above) is required.


## Configuration 

Woodpecker needs to know it's own address: 

public address of it in <scheme>://<hostname> format. Please omit trailing slashes:

# Connect to docker

As agents run pipeline steps as docker containers they require access to the host machine's Docker daemon:

# docker-compose.yml
version: '3'

services:
  [...]
  woodpecker-agent:
    [...]
+   volumes:
+     - /var/run/podman.sock:/var/run/docker.sock


# TODO 

use podman 
use DB 


#default port is 9000 or 8000 ? 

## Helpful Ressources 

https://containers.fan/posts/setup-gitea-with-woodpecker-ci/
https://github.com/ruanbekker/woodpecker-ci-demo/blob/main/docker-compose.yml