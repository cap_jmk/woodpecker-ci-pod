echo Please enter the amount of agents you want to launch: \

read agents

for ((i = 1; i <= agents; i++))
do
podman run \
    -d \
    --pod woodpecker\
    --name woodpecker-agent-${i} \
    --secret=woodpecker-agent-secret,type=env,target=WOODPECKER_AGENT_SECRET \
    --restart=always \
    -e WOODPECKER_SERVER=localhost:8000 \
    -v /home/pi/woodpecker-ci-pod/woodpecker-agent-config:/etc/woodpecker \
    -v /var/run/podman/podman.sock:/var/run/docker.sock \
    docker.io/woodpeckerci/woodpecker-agent:latest
done