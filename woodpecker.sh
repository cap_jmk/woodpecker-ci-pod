podman run \
    -d \
    --pod woodpecker \
    --name woodpecker-server \
    --restart=always \
    --secret=woodpecker-agent-secret,type=env,target=WOODPECKER_AGENT_SECRET \
    --secret=woodpecker-forgejo-secret,type=env,target=WOODPECKER_GITEA_SECRET \
    -e WOODPECKER_HOST=https://sail-ci \
    -e WOODPECKER_GITEA=true \
    -e WOODPECKER_GITEA_URL=https://gitea \
    -e WOODPECKER_GITEA_CLIENT=gitea-secret \
    -e WOODPECKER_DEBUG_PRETTY=true \
    -v /home/pi/woodpecker-ci-pod/woodpecker-server-data:/var/lib/woodpecker/ \
    docker.io/woodpeckerci/woodpecker-server:latest